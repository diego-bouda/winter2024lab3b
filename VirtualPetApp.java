import java.util.Scanner;

public class VirtualPetApp {
	public static void main(String[] args){
		
		Bird[] flock = new Bird[4];
		
		for(int i = 0; i < flock.length; i++){
			Scanner reader = new Scanner(System.in);
			System.out.println("give a name to the bird");
			String name = reader.nextLine();
		
			System.out.println("does the bird eat seeds or worms?");
			String food = reader.nextLine();
			
			System.out.println("what color is the bird?");
			String colour = reader.nextLine();
			
			flock[i] = new Bird(name, food, colour);
		}
		System.out.println(flock[3].name);
		System.out.println(flock[3].food);
		System.out.println(flock[3].colour);
		System.out.println(flock[0].isVegeterian());
		System.out.println(flock[0].colourOfFeathers());
	}
}
