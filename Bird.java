public class Bird{

	// **The indentation is fine on my original .java file but it becomes weird when I put it in gitlab, please look at the screenshot in the word**
	public String name;
    public String food;
    public String colour; 
	
	public Bird(String name, String food, String colour){
        this.name = name;
        this.food = food;
        this.colour = colour;
    }
	public String isVegeterian(){
		if(this.food.equals("seeds")){
			return "I am vegetarian!";
		}else{
			return "I am not vegetarian";
		}
	}
	public String colourOfFeathers(){
		return "My feathers are " + this.colour + " !"; 
	}
}
